from django.apps import AppConfig


class SiteblogappConfig(AppConfig):
    name = 'siteBlogApp'
